FROM alpine:latest AS builder

RUN apk add \
        build-base \
        gnupg \
        openssl \
        pcre-dev \
        zlib

ARG VERSION=1.14.2

RUN wget https://nginx.org/download/nginx-${VERSION}.tar.gz && \
    tar -xf nginx-${VERSION}.tar.gz

WORKDIR /nginx-${VERSION}

RUN apk add libressl-dev

RUN ./configure \
    --with-ld-opt="-static" \
    --with-http_sub_module \
    --with-http_ssl_module \
    --without-http_gzip_module && \
    make install

FROM scratch
COPY --from=builder /usr/local/nginx /usr/local/nginx
COPY --from=builder /etc/passwd /etc/group /etc/

EXPOSE 80
ENTRYPOINT ["/usr/local/nginx/sbin/nginx"]
CMD ["-g", "daemon off;"]
